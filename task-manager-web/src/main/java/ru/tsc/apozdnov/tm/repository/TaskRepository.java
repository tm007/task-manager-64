package ru.tsc.apozdnov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.tsc.apozdnov.tm.model.Task;

import java.util.*;

@Repository
public class TaskRepository {

    @NotNull
    private final Map<String, Task> tasks = new HashMap<>();

    {
        add("Task-" + UUID.randomUUID());
        add("Task-" + UUID.randomUUID());
        add("Task-" + UUID.randomUUID());
    }


    public void add(@NotNull final String name) {
        final Task task = new Task(name);
        tasks.put(task.getId(), task);
    }

    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }

    @NotNull
    public List<Task> findAll() {
        return new ArrayList<>(tasks.values());
    }

    @NotNull
    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public void save(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

}