package ru.tsc.apozdnov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.tsc.apozdnov.tm.model.Project;

import java.util.*;


@Repository
public class ProjectRepository {

    @NotNull
    private final Map<String, Project> projects = new HashMap<>();

    {
        add("Project-" + UUID.randomUUID());
        add("Project-" + UUID.randomUUID());
        add("Project-" + UUID.randomUUID());
        add("Project-" + UUID.randomUUID());
        add("Project-" + UUID.randomUUID());

    }


    public void add(@NotNull final String name) {
        final Project project = new Project(name);
        projects.put(project.getId(), project);
    }

    public void removeById(@NotNull final String id) {
        projects.remove(id);
    }

    @NotNull
    public List<Project> findAll() {
        return new ArrayList<>(projects.values());
    }

    @NotNull
    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void save(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

}